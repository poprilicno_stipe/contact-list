import { Contact } from '../models/contact'

export const contactsData: Array<Contact> = [
    {
        id: 1,
        firstName: 'Addie',
        lastName: 'Hernandez',
        favourite: false,
        image: '../../assets/images/CBC0BAE7-9AAE-4A24-A942-7F07D52F502E.png',
        phoneNumbers: [
            { title: 'home', number: '+38521546456' },
            { title: 'work', number: '+38521147258' },
            { title: 'cell', number: '+38596547541' },
            { title: 'husband', number: '+38599456214' }
        ],
        email: 'addie.hernandez@gmail.com',
    },
    {
        id: 2,
        firstName: 'Oscar',
        lastName: 'Arnold',
        favourite: false,
        image: '../../assets/images/96569EF1-FD47-4648-A2C5-EB9A447AEB7B.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'oscar.arnold@gmail.com',
    },
    {
        id: 3,
        firstName: 'Isaiah',
        lastName: 'McGuire',
        favourite: false,
        image: '../../assets/images/9D910EB6-6662-4BFE-8BB1-0D93918071CA.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'isaiah.mcguire@gmail.com',
    },
    {
        id: 4,
        firstName: 'Ann',
        lastName: 'Schneider',
        favourite: true,
        image: '../../assets/images/A565E1FF-F470-427D-897B-EB8200B23DBD.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'ann.schneider@gmail.com',
    },
    {
        id: 5,
        firstName: 'Agnes',
        lastName: 'Terry',
        favourite: true,
        image: '../../assets/images/9882F26E-DC1F-4193-8B7D-6F56C95AEE45.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'agnes.terry@gmail.com',
    },
    {
        id: 6,
        firstName: 'Rose',
        lastName: 'Bush',
        favourite: true,
        image: '../../assets/images/DA7258C2-2C3E-452C-B036-8B3C52DCACE1.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'rose.bush@gmail.com',
    },
    {
        id: 7,
        firstName: 'Duane',
        lastName: 'Reese',
        favourite: false,
        image: '../../assets/images/E4A0843E-24A9-4EE6-B173-A3150258159A.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'duane.reese@gmail.com',
    },
    {
        id: 8,
        firstName: 'Mae',
        lastName: 'Chandler',
        favourite: false,
        image: '../../assets/images/F4411F7C-A731-410C-8F16-E63A24D4E7F2.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'mae.chandler@gmail.com',
    },
    {
        id: 9,
        firstName: 'Evelyn',
        lastName: 'Weaver',
        favourite: false,
        image: '../../assets/images/FA38534F-B5BF-4B9B-9281-2C5D59280C76.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'evelynweaver@gmail.com',
    },
    {
        id: 10,
        firstName: 'Catherine',
        lastName: 'Moore',
        favourite: true,
        image: '../../assets/images/8393F051-6436-4DDF-829E-BA841C4A2695.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'catherine.moore@gmail.com',
    },
    {
        id: 11,
        firstName: 'Sam',
        lastName: 'Manning',
        favourite: false,
        image: '../../assets/images/3C237868-BE4F-490D-8B37-443184CC9702.png',
        phoneNumbers: [
            { title: 'cell', number: '+38521546456' },
            { title: 'work', number: '+38521147258' }
        ],
        email: 'sam.manning@gmail.com',
    }
];
