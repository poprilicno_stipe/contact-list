import { Pipe, PipeTransform } from '@angular/core';
import { Contact } from '../models/contact';

@Pipe({
  name: 'contactsFilter',
  pure: false

})
export class ContactsFilterPipe implements PipeTransform {

  transform(
    contacts: Contact[],
    contactFilterValue: string,
    showOnlyFavourites: boolean
  ): Contact[] {

    if (!contacts) return [];

    contactFilterValue = contactFilterValue.toLocaleLowerCase();
    contacts = [...contacts.filter(item => item.firstName.toLocaleLowerCase().includes(contactFilterValue) || item.lastName.toLocaleLowerCase().includes(contactFilterValue))];

    if (showOnlyFavourites) contacts = [...contacts.filter(item => item.favourite === true)];
    
    return contacts;
  }

}
