import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input() contactSearchValue: string;
  @Output() contactSearchValueChange = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onModelChange(value: string) {
    this.contactSearchValueChange.emit(value);
  }

}
