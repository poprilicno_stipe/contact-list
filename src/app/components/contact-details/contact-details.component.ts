import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { Contact } from 'src/app/models/contact';
import { ContactsService } from '../../services/contacts.service';


@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {
  contact$: Observable<Contact>;

  constructor(
    private route: ActivatedRoute,
    private contactsService: ContactsService
  ) { }

  ngOnInit() {
    let x;
    this.contact$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.contactsService.getContact(params.get('id')))
    );
  }

}
