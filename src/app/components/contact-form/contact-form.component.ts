import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { switchMap } from 'rxjs/operators';

import { Contact } from 'src/app/models/contact';
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})

export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;
  contact$: Observable<Contact>;
  contactItem: Contact;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private contactsService: ContactsService) { }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.initializeEmptyContactForm();
        this.getContactData();
        this.contact$.subscribe(res => this.contactItem = res);
        this.initializeContactForm();
      } else {
        this.initializeEmptyContactForm();
      }
    });
  }

  getContactData() {
    this.contact$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.contactsService.getContact(params.get('id')))
    );
  }

  initializeContactForm() {
    this.contactForm.patchValue({
      id: [this.contactItem.id],
      fullName: [this.contactItem.firstName + ' ' + this.contactItem.lastName],
      firstName: [this.contactItem.firstName],
      lastName: [this.contactItem.lastName],
      favourite: [this.contactItem.favourite],
      image: [this.contactItem.image],
      email: [this.contactItem.email]
    });
    //    this.addPreloadPhoneNumbers();
    //this.contactForm.setControl('phoneNumbers', this.fb.array(this.contactItem.phoneNumbers || []));
  }

  initializeEmptyContactForm() {
    this.contactForm = this.fb.group({
      id: [],
      fullName: [''],
      firstName: [''],
      lastName: [''],
      favourite: [],
      image: [],
      email: [],
      phoneNumbers: this.fb.array([this.addOtherPhoneNumbers()])
    });
  }
  /* 
    addPreloadPhoneNumbers(): FormGroup {
      this.contactForm.setControl('phoneNumbers', this.fb.array(this.contactItem.phoneNumbers || []));
      console.log(this.contactForm);
      return
    } */

  addOtherPhoneNumbers(): FormGroup {
    return this.fb.group({
      title: [''],
      number: ['']
    });
  }

  addPhoneNumberInput(): void {
    (<FormArray>this.contactForm.get('phoneNumbers')).push(this.addOtherPhoneNumbers());
  }

  deletePhoneNumberInput(index: number) {
    const control = <FormArray>this.contactForm.controls.phoneNumbers;
    control.removeAt(index);
  }

  onSubmit() {
    // todo: push this contactForm array into main data array contactsData
    // todo: image upload thing 
    // there is no favourite for some reason
    console.log(this.contactForm);
  }
}

