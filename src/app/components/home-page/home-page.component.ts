import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Contact } from 'src/app/models/contact';
import { contactsData } from '../../data/contactsData';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  contacts: Observable<Contact[]>;
  contactFilterValue: string = '';
  showOnlyFavourites: boolean = false;

  constructor() { }

  ngOnInit() {
    this.contacts = of(contactsData);
  }

  showFavourite(isFavourite: boolean) {
    if (isFavourite === true) {
      this.showOnlyFavourites = true;
    } else if (isFavourite === false) {
      this.showOnlyFavourites = false;
    }
  }
}
