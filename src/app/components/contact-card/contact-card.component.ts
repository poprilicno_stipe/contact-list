import { Component, OnInit, Input} from '@angular/core';

import { Contact } from '../../models/contact'
import { ContactsService } from '../../services/contacts.service';

@Component({
  selector: 'app-contact-card',
  templateUrl: './contact-card.component.html',
  styleUrls: ['./contact-card.component.css']
})
export class ContactCardComponent implements OnInit {
  @Input() contact: Contact;

  constructor(private contactsService: ContactsService) { }

  ngOnInit() {
  }

  deleteContact(contact) {
    this.contactsService.removeContactFromList(contact);
    console.log(contact);
  }

}



