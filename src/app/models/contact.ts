import { ContactInterface, PhoneNumbersInterface } from './interfaces';

export class Contact implements ContactInterface {
    id: number;
    firstName: string;
    lastName: string;
    favourite: boolean;
    image: string;
    phoneNumbers: Array<PhoneNumbersInterface>;
    email: string;
}
