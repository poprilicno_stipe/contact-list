export interface ContactInterface {
    id: number;
    firstName: string;
    lastName: string;
    favourite: boolean;
    image?: string;
}

export interface PhoneNumbersInterface {
    title: string,
    number: string
}