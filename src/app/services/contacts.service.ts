import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { Contact } from '../models/contact';
import { contactsData } from '../data/contactsData';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor() { }

  getContactsData(): Observable<Contact[]> {
    return of(contactsData);
  }

  getContact(id: number | string) {
    return this.getContactsData().pipe(
      // (+) before `id` turns the string into a number
      map((contacts: Contact[]) => contacts.find(contact => contact.id === +id))
    );
  }

  removeContactFromList(contact: Contact) {
    let index = contactsData.indexOf(contact);
    contactsData.splice(index, 1);
    console.log(contactsData);
  }

}
